# This is a practice on how to use mySQL

## Install

1. Installation see: [https://gitlab.cern.ch/weiy/centos_windows_dual_system_configuration/-/blob/master/centos/software/mysql/README.md](https://gitlab.cern.ch/weiy/centos_windows_dual_system_configuration/-/blob/master/centos/software/mysql/README.md)

## common commands:

1. more can be found here [./doc/MySQL_notes.pdf](./doc/MySQL_notes.pdf) 

1. login:

    - mysql -h dbod-sctprod.cern.ch --port=5500 -u sctreader -ppassword

1. etc:
    - create database: `mysql> create database DATABESE_NAME;`

    - use databases: `mysql> use DATABASE_NAME;`

    - exit mysql: `mysql> exit`
    
    <br>

1. show:

    - show databases: `mysql> show databases;` 

	- show current databases used: `mysql> select database();`
	
    - show tables: `mysql> show tables;`

    - show tables from other database: `mysql> show tables from OTHER_DATABASE_NAME;`

    - show structure of a table: `mysql> desc TABLE_NAME;`

    <br>

1. select:

    - select an entity from a table: `mysql> select ENTITY_NAME from TABLE_NAME;`

    - select mutiple entities from a table: `mysql> select ENTITY_NAME1, ENTITY_NAME2 from TABLE_NAME;`

    - select all entities from a table: `mysql> select * from TABLE_NAME;`

    - select with requirements: `mysql> select ENTITY_NAME1, ENEITY_NAME2 from TABLE_NAME where ENEITY_NAME2 > 20;`

1. order:

    - select and order by one ENTITY: `mysql> select ENTITY_NAME1, ENTITY_NAME2 order by ENTITY_NAME1;`

    - note that: `order by` should be put after `where`

    - order asc: `mysql> select ENTITY_NAME1, ENTITY_NAME2 order by ENTITY_NAME1 asc;`

    - order desc: `mysql> select ENTITY_NAME1, ENTITY_NAME2 order by ENTITY_NAME1 desc;`


## database import and export

1. database export: 

	- `mysqldump --single-transaction -h XXXX.hostname --port=PORT -u USER -ppassword --all-databases > all_db_backup.sql`

1. database import:

	- `mysql -u root -p < all_db_backup.sql`
